module My
  module Module
    module Is
      module Great
        def self.included(klass)
          klass.class_eval do
            before_filter :filter_me

            def filter_me
              raise "I DO NOT LIKE FAILURES!" if params.has_key? :failure
            end
          end
        end
      end
    end
  end
end
ActionController::Base.send(:include, My::Module::Is::Great)